from django.urls import path, include
from .views import CarsList, RentsList, RentList

urlpatterns = [
    # path to djoser end points
    path('auth/', include('djoser.urls')),
    path('auth/', include("djoser.urls.authtoken")),
    path('cars/', CarsList.as_view()),
    path('rents/', RentsList.as_view()),
    path('rent/', RentList.as_view()),
]
