from django.db import models
from django.contrib.auth.models import User


class Car(models.Model):
    name = models.CharField(max_length=50, blank=False)
    price = models.IntegerField(blank=False)


class CarImage(models.Model):
    name = models.CharField(max_length=100)
    file = models.FileField()
    car = models.ForeignKey(
        Car, to_field='id', on_delete=models.CASCADE, related_name='image')


class Rent(models.Model):
    car = models.ForeignKey(Car, on_delete=models.CASCADE,
                            to_field='id', related_name="car")
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, to_field='id', related_name="user")
