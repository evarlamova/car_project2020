from django.apps import AppConfig


class RentApiConfig(AppConfig):
    name = 'rent_api'
