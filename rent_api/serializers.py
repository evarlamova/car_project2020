from .models import Car, CarImage, Rent
from rest_framework import serializers
from djoser.serializers import UserSerializer

class FileUploadSerializer(serializers.ModelSerializer):

    class Meta:
        model = CarImage
        fields = '__all__'


class CarSerializer(serializers.ModelSerializer):
    image = FileUploadSerializer(many=True)

    class Meta:
        model = Car
        fields = ('id', 'name', 'price', 'image')


class RentsSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    car = CarSerializer()

    class Meta:
        model = Rent
        fields = ('id', 'car', 'user')


class RentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rent
        fields = '__all__'
