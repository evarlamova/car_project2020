from rest_framework import generics
from .models import Car, Rent
from .serializers import CarSerializer, RentSerializer, RentsSerializer
from rest_framework.response import Response
from rest_framework import status


class CarsList(generics.ListCreateAPIView):
    queryset = Car.objects.all()
    serializer_class = CarSerializer


class RentsList(generics.ListCreateAPIView):
    serializer_class = RentsSerializer

    def get(self, request):
        user = request.user
        request = Rent.objects.filter(user=user)
        if not len(request):
            return Response(status=status.HTTP_204_NO_CONTENT)

        serializer = RentsSerializer(request, many=True)

        if not len(serializer.data):
            return Response(
                {"data": []},
                status=status.HTTP_200_OK,
            )

        return Response(serializer.data)

    def delete(self, request):
        idForDelete = request.data['carId']

        rent = Rent.objects.get(pk=idForDelete)
        rent.delete()

        return Response(status=status.HTTP_200_OK)


class RentList(generics.ListCreateAPIView):
    queryset = Rent.objects.all()
    serializer_class = RentSerializer
